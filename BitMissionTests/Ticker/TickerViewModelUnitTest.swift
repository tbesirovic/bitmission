//
//  BitMissionTests.swift
//  BitMissionTests
//
//  Created by Timur Besirovic on 09/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import XCTest
@testable import BitMission

class TickerViewModelUnitTest: XCTestCase {
    
    let simulatedJSONObject = ["123", "21.23", "8.88", "1.24", "3.42", "12.3"]
    let currencyChannelName = "USDT_LTC" // For ticker id == 123 ticker name must be USDT_LTC
    
    
    override func setUp() { }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTickerModelAndTickerViewModel() {
        let ticker = TickerModel(with: simulatedJSONObject)
        let tickerViewModel = TickerViewModel(withTicker: ticker)
        
        if let lastTradePrice = Double(simulatedJSONObject[1]),
           let lowestAsk = Float(simulatedJSONObject[2]),
           let highestBid = Float(simulatedJSONObject[3]),
           let percentChangeInLastDay = Float(simulatedJSONObject[4]),
           let baseCurrencyVolume = Double(simulatedJSONObject[5]) {
            XCTAssertEqual(simulatedJSONObject[0], tickerViewModel.id)
            XCTAssertEqual(currencyChannelName, tickerViewModel.currencyChannels[tickerViewModel.id])
            XCTAssertEqual(lastTradePrice, tickerViewModel.lastTradePrice)
            XCTAssertEqual(percentChangeInLastDay * 100 , tickerViewModel.percentChangeInLastDay)
            XCTAssertEqual(lowestAsk, tickerViewModel.lowestAsk)
            XCTAssertEqual(highestBid, tickerViewModel.highestBid)
            XCTAssertEqual(baseCurrencyVolume, tickerViewModel.baseCurrencyVolume)
        } else {
            XCTFail("TickerUnitTest - testTickerModelAndTickerViewModel -> Cast failed")
        }
    }
    
}
