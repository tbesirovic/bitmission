//
//  RegistrationViewModel.swift
//  BitMissionTests
//
//  Created by Timur Besirovic on 09/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import XCTest
@testable import BitMission

class RegistrationViewModelUnitTest: XCTestCase {

    var tmpFullName = "John Smith"
    var tmpEmail = "test@gmail.com"
    var tmpPassword = "12345678"
    let registrationViewModel = RegistrationViewModel()
    
    override func setUp() {
        registrationViewModel.fullName = tmpFullName
        registrationViewModel.email = tmpEmail
        registrationViewModel.password = tmpPassword
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testRegistrationViewModel() {
        XCTAssertEqual(tmpFullName, registrationViewModel.fullName)
        XCTAssertEqual(tmpEmail, registrationViewModel.email)
        XCTAssertEqual(tmpPassword, registrationViewModel.password)
    }
    
    
}
