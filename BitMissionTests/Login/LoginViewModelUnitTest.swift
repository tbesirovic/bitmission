//
//  LoginViewModelUnitTest.swift
//  BitMissionTests
//
//  Created by Timur Besirovic on 09/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import XCTest
@testable import BitMission

class LoginViewModelUnitTest: XCTestCase {

    let tmpEmail = "test@gmail.com"
    let tmpPassword = "12345678"
    let loginViewModel = LoginViewModel()
    
    override func setUp() {
        loginViewModel.email = tmpEmail
        loginViewModel.password = tmpPassword
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLoginViewModel() {
        XCTAssertEqual(tmpEmail, loginViewModel.email)
        XCTAssertEqual(tmpPassword, loginViewModel.password)
    }
    
}
