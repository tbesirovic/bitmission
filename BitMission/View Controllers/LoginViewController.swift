//
//  LoginViewController.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import UIKit

final class LoginViewController: UIViewController {
    
    //
    // MARK: Dependencies
    //
    
    private let userService = UserService.user
    private let apiService = ApiService.shared
    
    //
    // MARK: Properties
    //
    
    private let loginViewModel = LoginViewModel()
    
    //
    // MARK: - UI properties
    //
    
    private let activityView = UIActivityIndicatorView()

    private let emailTextField: CustomTextField = {
        let tf = CustomTextField(placeholder: BMString.enterEmail)
        tf.keyboardType = .emailAddress
        tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return tf
    }()
    
    private let passwordTextField: CustomTextField = {
        let tf = CustomTextField(placeholder: BMString.enterPassword)
        tf.isSecureTextEntry = true
        tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return tf
    }()
    
    private let loginButton: CustomButton = {
        let btn = CustomButton(type: .system)
        btn.setTitle(BMString.login, for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return btn
    }()
    
    private let goToRegisterButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(BMString.imANewMember, for: .normal)
        button.setTitleColor(BMColors.brandLight, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .heavy)
        button.addTarget(self, action: #selector(hendleGoToRegister), for: .touchUpInside)
        return button
    }()
    
    private lazy var verticalStackView = UIStackView(arrangedSubviews: [emailTextField,
                                                                passwordTextField,
                                                                loginButton,
                                                                ])
    
    
    //
    // MARK: View's lifecycle
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLayout()
        setupKeyboadDismissTapGesture()
        setupLoginViewModelObservers()
        setupActivityIndicator()
    }
    
    //
    // MARK: UI setup methods
    //
    
    private func setupLayout() {
        navigationController?.isNavigationBarHidden = true
        
        view.backgroundColor = BMColors.brand
        self.view.addSubview(verticalStackView)
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 10
        verticalStackView.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 50, bottom: 0, right: 50))
        verticalStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        view.addSubview(goToRegisterButton)
        goToRegisterButton.anchor(top: nil, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor)
    }
    
    @objc private func hendleGoToRegister() {
        let registrationController = RegistrationViewController()
        navigationController?.pushViewController(registrationController, animated: true)
    }
    
    @objc private func handleLogin() {
        guard apiService.internetConnectionStatus.connection != .none else {
            let alert = UIAlertController.getSimpleAlert(withMessage: BMString.errorNoInternetConnection)
            self.present(alert, animated: true, completion: nil)
            return
        }
        loginViewModel.performLogin { isSuccess, errorMessage in
            guard errorMessage == nil else {
                self.activityView.stopAnimating()
                let alert = UIAlertController.getSimpleAlert(withMessage: errorMessage ?? "-")
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.userService.saveUserLoginState(isLoggedIn: true)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //
    // MARK: Custom methods
    //
    
    private func setupActivityIndicator() {
        activityView.style = .whiteLarge
        activityView.center = self.view.center
        self.view.addSubview(activityView)
    }
    
    private func setupKeyboadDismissTapGesture() {
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapDismiss)))
    }
    
    private func setupLoginViewModelObservers() {
        loginViewModel.isFormValidObserver = { [unowned self] (isFormValid) in
            self.loginButton.isEnabled = isFormValid
            isFormValid ? self.loginButton.setState(isEnabled: true) : self.loginButton.setState(isEnabled: false)
        }
        
        loginViewModel.isLoggingInObserver = { [unowned self] (isRegistering) in
            isRegistering ? self.activityView.startAnimating() : self.activityView.stopAnimating()
        }
    }
    
    //
    // MARK: Selector methods
    //
    
    @objc private func handleTextChange(textField: UITextField) {
        if textField == emailTextField {
            loginViewModel.email = textField.text
        } else {
            loginViewModel.password = textField.text
        }
    }
    
    @objc private func handleTapDismiss() {
        view.endEditing(true)
    }
    
}
