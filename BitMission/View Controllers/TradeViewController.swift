//
//  TradeViewController.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import UIKit
import SwiftWebSocket

final class TradeViewController: UIViewController {
    
    //
    // MARK: Dependencies
    //
    
    private let apiService = ApiService.shared
    private let userService = UserService.user
    
    //
    // MARK: Properties
    //
    
    private var dataArray: [TickerViewModel] = []
    private var cells: [BaseTradeTableViewCell] = []
    private let activityView = UIActivityIndicatorView()
    
    //
    // MARK: UI properies
    //
    
    private let limitValueField: UITextField = {
        let textfield = CustomTextField(placeholder: BMString.enterLimitPlaceHolder)
        textfield.keyboardType = .numbersAndPunctuation
        textfield.textColor = BMColors.brandLight
        return textfield
    }()
    
    private let segmentedControl: UISegmentedControl = {
        let items = [BMString.defaultView, BMString.customView]
        let filtersSegment = UISegmentedControl(items: items)
        filtersSegment.tintColor = BMColors.brandLight
        filtersSegment.addTarget(self, action: #selector(filterApply(segment:)), for: .valueChanged)
        return filtersSegment
        }()
    
    private let tableView: UITableView = {
        let tableview = UITableView(frame: .zero)
        tableview.backgroundColor = BMColors.brandSuperLight
        return tableview
    }()
    
    //
    // MARK: View's lifecycle
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForNotifications()
        setupNavigationItems()
        registerCells()
        setupTableView()
        setupViewElements()
        setupActivityIndicator()
        updateSegmentedControllCurrentState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presentViewController()
    }
    
    //
    // MARK: - UI setup methods
    //
    
    private func setupViewElements() {
        limitValueField.delegate = self
        
        let valueView = UIView()
        valueView.backgroundColor = BMColors.brand
        valueView.addSubview(limitValueField)
        valueView.addSubview(segmentedControl)
        
        limitValueField.anchor(top: valueView.topAnchor, leading: valueView.leadingAnchor, bottom: valueView.bottomAnchor, trailing: valueView.trailingAnchor, padding: .init(top: 50, left: 15, bottom: 4, right: 15))
        
        segmentedControl.anchor(top: valueView.topAnchor, leading: valueView.leadingAnchor, bottom: valueView.bottomAnchor, trailing: valueView.trailingAnchor, padding: .init(top: 0, left: 15, bottom: 50, right: 15))
        
        view.addSubview(valueView)
        valueView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, size: .init(width: 0, height: 92))
        
        view.addSubview(tableView)
        tableView.anchor(top: valueView.bottomAnchor, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor)
    }
    
    private func setupNavigationItems() {
        navigationController?.navigationBar.isTranslucent = false;
        navigationController?.navigationBar.barTintColor = BMColors.brand
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.title = BMString.tradeHighlight
        
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusBar.backgroundColor = BMColors.brand
        }
        view.backgroundColor = BMColors.brand
        
        let fontSize:CGFloat = 13
        let font:UIFont = UIFont.boldSystemFont(ofSize: fontSize);
        let attributes:[NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): font]
        
        let rightBarButtonItem = UIBarButtonItem(title: BMString.logOut, style: .plain, target: self, action: #selector(handleLogout))
        rightBarButtonItem.tintColor = .white
        rightBarButtonItem.setTitleTextAttributes(attributes, for: .normal)
        navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    //
    // MARK: Custom methods
    //
    
    private func updateSegmentedControllCurrentState() {
        let currentTradeStyle = userService.getCurrentTradeStyle() ? 1 : 0
        segmentedControl.selectedSegmentIndex = currentTradeStyle
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(internetConnectionChanged), name: .reachabilityChanged, object: nil)
    }
    
    private func presentViewController() {
        userService.isLogged() ? startListeningWebSocket() : presentLoginViewController()
    }
    
    private func presentLoginViewController() {
        let loginViewController = LoginViewController()
        let navController = UINavigationController(rootViewController: loginViewController)
        self.present(navController, animated: true)
    }
    
    private func registerCells(){
        tableView.register(UINib(nibName: DefaultTickerTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: DefaultTickerTableViewCell.reuseIdentifier)
        tableView.register(UINib(nibName: CustomTickerTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: CustomTickerTableViewCell.reuseIdentifier)
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func setupActivityIndicator() {
        activityView.style = .whiteLarge
        activityView.color = .gray
        activityView.center = self.view.center
        self.view.addSubview(activityView)
    }
    
    private func startListeningWebSocket(){
        activityView.startAnimating()
        apiService.subscribeWebSocket(fromView: self.view) { (ticker) in
            if let cTicker = self.dataArray.first(where: {$0.id == ticker.id}) {
                cTicker.lastTradePrice = ticker.lastTradePrice
                cTicker.lowestAsk = ticker.lowestAsk
                cTicker.highestBid = ticker.highestBid
                cTicker.baseCurrencyVolume = ticker.baseCurrencyVolume
            } else {
                var cell: BaseTradeTableViewCell!
                if self.userService.getCurrentTradeStyle() {
                    cell = Bundle.main.loadNibNamed(CustomTickerTableViewCell.nibName, owner: self, options: nil)?[0] as! CustomTickerTableViewCell
                } else {
                    cell = Bundle.main.loadNibNamed(DefaultTickerTableViewCell.nibName, owner: self, options: nil)?[0] as! DefaultTickerTableViewCell
                }
                self.cells.append(cell)
                self.dataArray.append(TickerViewModel(withTicker: ticker))
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: [IndexPath.init(row: self.dataArray.count-1, section: 0)], with: .automatic)
                self.tableView.endUpdates()
                self.activityView.stopAnimating()
            }
        }
    }
    
    //
    // MARK: - Selector methods
    //
    
    @objc private func filterApply(segment: UISegmentedControl) -> Void {
        cells.removeAll()
        let currentTradeStyle = userService.changeTradeStyle()
        var cell: BaseTradeTableViewCell!
        dataArray.forEach {_ in
            if currentTradeStyle {
                cell = Bundle.main.loadNibNamed(CustomTickerTableViewCell.nibName, owner: self, options: nil)?[0] as! CustomTickerTableViewCell
                tableView.separatorStyle = .none
            } else {
                cell = Bundle.main.loadNibNamed(DefaultTickerTableViewCell.nibName, owner: self, options: nil)?[0] as! DefaultTickerTableViewCell
                tableView.separatorStyle = .singleLine
            }
            self.cells.append(cell)
        }
        tableView.reloadData()
    }
    
    @objc private func appDidBecomeActive() {
        startListeningWebSocket()
    }
    
    @objc private func appDidEnterBackground() {
        apiService.unsubscribeWebSocket()
    }
    
    @objc private func internetConnectionChanged() {
        switch apiService.internetConnectionStatus.connection {
        case .none:
            apiService.unsubscribeWebSocket()
            let alert = UIAlertController.getSimpleAlert(withMessage: BMString.errorNoInternetConnection)
            self.present(alert, animated: true, completion: nil)
        default:
            startListeningWebSocket()
        }
    }
    
    @objc private func handleLogout() {
        FirebaseAuthService().logoutUser { isSuccess, errorMessage in
            guard isSuccess else {
                let alert = UIAlertController.getSimpleAlert(withMessage: errorMessage ?? "-")
                self.present(alert, animated: true, completion: nil)
                return
            }
            apiService.unsubscribeWebSocket()
            userService.saveUserLoginState(isLoggedIn: false)
            presentLoginViewController()
        }
    }
}

//
// MARK: Extension - UITextFieldDelegate
//

extension TradeViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch string {
        case "0","1","2","3","4","5","6","7","8","9":
            return true
        case ".":
            let array = Array(textField.text!)
            var decimalCount = 0
            for character in array {
                if character == "." {
                    decimalCount+=1
                }
            }
            if decimalCount == 1 {
                return false
            } else {
                return true
            }
        default:
            let array = Array(string)
            if array.count == 0 {
                return true
            }
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let _ = Double(textField.text!) {
            textField.resignFirstResponder()
            return true
        } else {
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.dataArray.forEach { (tickerViewModel) in
            if let value = Double(textField.text!) {
                tickerViewModel.updateLimitValue(newLimitValue: value)
            }
        }
    }
}

//
// MARK: Extension - UITableViewDelegate, UITableViewDataSource
//

extension  TradeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cells[indexPath.row]
        cell.tickerViewModel = dataArray[indexPath.row]
        return cell
    }
}
