//
//  RegistrationViewController.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import UIKit

final class RegistrationViewController: UIViewController {
    
    // MARK: Dependencies
    
    private let userService = UserService.user
    private let apiService = ApiService.shared
    
    //
    // MARK: Properties
    //
    
    private let registrationViewModel = RegistrationViewModel()
    
    //
    // MARK: UI properties
    //
    
    private let activityView = UIActivityIndicatorView()
    
    private let logoView: UIView = {
        let view = UIView()
        view.heightAnchor.constraint(equalToConstant: 98).isActive = true
        return view
    }()
    
    private let fullNameTextField: UITextField = {
        let tf = CustomTextField(placeholder: BMString.enterFullName)
        tf.addTarget(self, action: #selector(handleTextChanged), for: .editingChanged)
        return tf
    }()
    
    private let emailTextField: UITextField = {
        let tf = CustomTextField(placeholder: BMString.enterEmail)
        tf.keyboardType = .emailAddress
        tf.addTarget(self, action: #selector(handleTextChanged), for: .editingChanged)
        return tf
    }()
    
    private let passwordTextField: UITextField = {
        let tf = CustomTextField(placeholder: BMString.enterPassword)
        tf.isSecureTextEntry = true
        tf.addTarget(self, action: #selector(handleTextChanged), for: .editingChanged)
        return tf
    }()
    
    private let registerButton: CustomButton = {
        let btn = CustomButton(type: .system)
        btn.setTitle(BMString.register, for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.addTarget(self, action: #selector(handleRegistration), for: .touchUpInside)
        return btn
    }()
    
    private let goToLoginButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle(BMString.goBack, for: .normal)
        btn.setTitleColor(BMColors.brandLight, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .heavy)
        btn.addTarget(self, action: #selector(handleGoToLogin), for: .touchUpInside)
        return btn
    }()
    
    
    private lazy var verticalStackView = UIStackView(arrangedSubviews: [logoView,
                                                                        fullNameTextField,
                                                                        emailTextField,
                                                                        passwordTextField,
                                                                        registerButton])
    
    //
    // MARK: View's lifecycle
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        setupKeyboadNotificationObservers()
        setupKeyboadDismissTapGesture()
        setupRegistrationViewModelObserver()
        setupActivityIndicator()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    //
    // MARK: UI setup methods
    //
    
    fileprivate func setupLayout() {
        navigationController?.isNavigationBarHidden = true
        
        view.backgroundColor = BMColors.brand
        self.view.addSubview(verticalStackView)
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 10
        verticalStackView.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 50, bottom: 0, right: 50))
        verticalStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        view.addSubview(goToLoginButton)
        goToLoginButton.anchor(top: nil, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor)
    }
    
    //
    // MARK: Custom methods
    //
    
    private func setupRegistrationViewModelObserver() {
        registrationViewModel.isFormValidObserver = { [unowned self] (isFormValid) in
            self.registerButton.isEnabled = isFormValid
            isFormValid ? self.registerButton.setState(isEnabled: true) : self.registerButton.setState(isEnabled: false)
        }
        
        registrationViewModel.isRegistrationObserver = { [unowned self] (isRegistering) in
            if isRegistering == true {
                self.activityView.startAnimating()
            } else {
                self.activityView.stopAnimating()
            }
        }
    }
    
    private func setupActivityIndicator() {
        activityView.style = .whiteLarge
        activityView.center = self.view.center
        self.view.addSubview(activityView)
    }
    
    private func setupKeyboadDismissTapGesture() {
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapDismiss)))
    }
    
    private func setupKeyboadNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDismiss), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //
    // MARK: Selector methods
    //
    
    @objc fileprivate func handleTextChanged(textField: UITextField) {
        if textField == fullNameTextField {
            registrationViewModel.fullName = textField.text
        } else if textField == emailTextField {
            registrationViewModel.email = textField.text
        } else {
            registrationViewModel.password = textField.text
        }
    }
    
    @objc fileprivate func handleTapDismiss() {
        view.endEditing(true)
    }
    
    @objc fileprivate func handleKeyboardShow(notification: Notification) {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardFrame = value.cgRectValue
        let bottomSpace = view.frame.height - verticalStackView.frame.origin.y - verticalStackView.frame.height
        let difference = keyboardFrame.height - bottomSpace
        self.view.transform = CGAffineTransform(translationX: 0, y: -difference - 10)
    }
    
    @objc fileprivate func handleKeyboardDismiss() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.transform = .identity
        })
    }
    
    @objc fileprivate func handleRegistration() {
        guard apiService.internetConnectionStatus.connection != .none else {
            let alert = UIAlertController.getSimpleAlert(withMessage: BMString.errorNoInternetConnection)
            self.present(alert, animated: true, completion: nil)
            return
        }
        registrationViewModel.performRegistration { isSuccess, errorMessage in
            guard errorMessage == nil else {
                self.activityView.stopAnimating()
                let alert = UIAlertController.getSimpleAlert(withMessage: errorMessage ?? "-")
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.userService.saveUserLoginState(isLoggedIn: true)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc fileprivate func handleGoToLogin() {
        navigationController?.popViewController(animated: true)
    }
    
}
