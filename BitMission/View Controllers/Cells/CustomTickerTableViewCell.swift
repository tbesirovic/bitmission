//
//  CustomTickerTableViewCell.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import UIKit

final class CustomTickerTableViewCell: BaseTradeTableViewCell {
    
    //
    // MARK: Static properties
    //
    
    static var reuseIdentifier: String { return "CustomTickerTableViewCell" }
    static var nibName: String { return "CustomTickerTableViewCell" }
    
    //
    // MARK: IBOutlets
    //
    
    @IBOutlet private weak var bgView: UIView!
    @IBOutlet private weak var tickerName: UILabel!
    @IBOutlet private weak var lowestAsk: UILabel!
    @IBOutlet private weak var highestBid: UILabel!
    @IBOutlet private weak var baseCurrencyVolume: UILabel!
    
    //
    // MARK: Properties
    //
    
    override var tickerViewModel: TickerViewModel! {
        didSet {
            tickerName.text = tickerViewModel.tikerName
            lowestAsk.text = "\(BMString.ask): \(tickerViewModel.lowestAsk)"
            highestBid.text = "\(BMString.bid): \(tickerViewModel.highestBid)"
            baseCurrencyVolume.text = (String(format: "%.1f", tickerViewModel.baseCurrencyVolume))
            setupTickerViewModelObservers()
        }
    }
    
    //
    // MARK: Custom methods
    //
    
    private func setupTickerViewModelObservers() {
        tickerViewModel.lowestAskObserver = { [weak self] (newValue) in
            self?.lowestAsk.text = "\(BMString.ask): " + newValue
        }
        
        tickerViewModel.highestBidObserver = { [weak self] (newValue) in
            self?.highestBid.text = "\(BMString.bid): " + newValue
        }
    }
    
    //
    // MARK: View's lifecycle
    //
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = 12
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
}
