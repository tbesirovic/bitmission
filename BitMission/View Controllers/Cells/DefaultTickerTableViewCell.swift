//
//  DefaultTickerTableViewCell.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import UIKit

final class DefaultTickerTableViewCell: BaseTradeTableViewCell {
    
    //
    // MARK: Static properties
    //
    
    static var reuseIdentifier: String { return "DefaultTickerTableViewCell" }
    static var nibName: String { return "DefaultTickerTableViewCell" }
    
    //
    // MARK: IBOutlets
    //
    
    @IBOutlet private weak var tickerName: UILabel!
    @IBOutlet private weak var lastTradePrice: UILabel!
    @IBOutlet private weak var baseCurrencyVolume: UILabel!
    @IBOutlet private weak var tickerPercent: UILabel!
    @IBOutlet private weak var limitValueImage: UIImageView!
    
    //
    // MARK: Properties
    //
    
    override var tickerViewModel: TickerViewModel! {
        didSet {
            tickerName.text = tickerViewModel.tikerName
            lastTradePrice.text = "\(tickerViewModel.lastTradePrice)"
            baseCurrencyVolume.text = (String(format: "%.1f", tickerViewModel.baseCurrencyVolume))
            let persentage = String(format: "%.1f", tickerViewModel.percentChangeInLastDay)
            tickerViewModel.percentChangeInLastDay >= 0 ? (tickerPercent.textColor = .green) : (tickerPercent.textColor = .red)
            tickerPercent.text = persentage + "%"
            limitValueImage.image = UIImage()
            if let value = tickerViewModel.limitValue {
                value ? (limitValueImage.image = UIImage(named: BMString.RedArrowDown)) : (limitValueImage.image = UIImage(named: BMString.greenArrowUp))
            }
            setupTickerViewModelObservers()
        }
    }
    
    //
    // MARK: Custom methods
    //
    
    private func setupTickerViewModelObservers() {
        tickerViewModel.lastTradePriceObserver = { [weak self] (value, isGreater) in
            self?.lastTradePrice.text = value
            isGreater ? self?.blink(isHigher: true) : self?.blink(isHigher: false)
        }
        tickerViewModel.limitValueObserver = { [weak self] (isGreater) in
            isGreater ? (self?.limitValueImage.image = UIImage(named: BMString.RedArrowDown)) : (self?.limitValueImage.image = UIImage(named: BMString.greenArrowUp))
        }
        tickerViewModel.baseCurrencyVolumeObserver = { [weak self] (newValue) in
            self?.baseCurrencyVolume.text = newValue
        }
    }
    
    private func blink(isHigher: Bool) {
        backgroundColor = isHigher ? .green : .red
        UIView.animate(withDuration: 0.3, delay: 0.0, animations: {
            self.backgroundColor = .white
        }, completion: nil)
    }
    
    //
    // MARK: View's lifecycle
    //
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        limitValueImage.image = UIImage()
    }
    
}
