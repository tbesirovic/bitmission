//
//  BaseTradeTableViewCell.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import UIKit

class BaseTradeTableViewCell: UITableViewCell {

    //
    // MARK: Properties
    //
    
    var tickerViewModel: TickerViewModel!
    
    //
    // MARK: View lifecycle
    //
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = BMColors.brandSuperLight
    }
}
