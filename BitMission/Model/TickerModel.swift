//
//  TickerModel.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import Foundation

struct TickerModel {
    
    //
    // MARK: Properties
    //
    
    private(set) var id: String
    private(set) var lastTradePrice: Double
    private(set) var lowestAsk: Float
    private(set) var highestBid: Float
    private(set) var percentChangeInLastDay: Float
    private(set) var baseCurrencyVolume: Double
    
    //
    // MARK: Initializers
    //
    
    init(with data: [String]) {
        self.id = data[0]
        self.lastTradePrice = Double(data[1]) ?? 0
        self.lowestAsk = Float(data[2]) ?? 0
        self.highestBid = Float(data[3]) ?? 0
        self.percentChangeInLastDay = Float(data[4]) ?? 0
        self.baseCurrencyVolume = Double(data[5]) ?? 0
    }
    
}
