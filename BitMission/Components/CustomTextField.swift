//
//  CustomTextField.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import UIKit

final class CustomTextField: UITextField {

    //
    // MARK: Properties
    //
    
    let padding: CGFloat

    //
    // MARK: Initializers
    //
    
    init(padding: CGFloat = 16, placeholder: String) {
        self.padding = padding
        super.init(frame: .zero)
        self.placeholder = placeholder
        tintColor = .white
        layer.cornerRadius = 4
        layer.borderColor = BMColors.brandLight.cgColor
        layer.borderWidth = 1
        self.textColor = BMColors.brandSuperLight
    }
    
    //
    // MARK: Computed properties
    //
    
    override var intrinsicContentSize: CGSize {
        return .init(width: 0, height: 50)
    }
    
    //
    // MARK: Custom methods
    //
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding, dy: 0)
    }
    
    //
    // MARK: View's lifecycle
    //
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
