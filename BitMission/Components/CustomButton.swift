//
//  CustomButton.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import UIKit

final class CustomButton: UIButton {

    //
    // MARK: View's lifecycle
    //
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setTitleColor(BMColors.brandSuperLight, for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .heavy)
        backgroundColor = BMColors.brandLight.withAlphaComponent(0.7)
        isEnabled = false
        heightAnchor.constraint(equalToConstant: 44).isActive = true
        layer.cornerRadius = 8
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //
    // MARK: - Custom methods
    //
    
    func setState(isEnabled value: Bool) {
        backgroundColor = value ? BMColors.brandLight : BMColors.brandLight.withAlphaComponent(0.5)
        let titleColor = value ? BMColors.brandSuperLight : BMColors.brandSuperLight.withAlphaComponent(0.5)
        setTitleColor(titleColor, for: .normal)
    }
    
}
