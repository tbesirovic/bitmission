//
//  UserService.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import Foundation

final class UserService {
    
    static let user = UserService()
    
    private init() {}
    
    //
    // MARK: Properties
    //
    
    let kUserLogged = "userLogged"
    let kUserTradeStyle = "userTradeStyle"
    
    //
    // MARK: Custom methods
    //
        
    func saveUserLoginState(isLoggedIn loggedIn: Bool) {
        let valueToSave = loggedIn ? true : false
        UserDefaults.standard.set(valueToSave, forKey: kUserLogged)
    }
    
    func isLogged() -> Bool {
        return UserDefaults.standard.bool(forKey: kUserLogged)
    }
    
    func changeTradeStyle() -> Bool {
        UserDefaults.standard.set(!getCurrentTradeStyle(), forKey: kUserTradeStyle)
        return UserDefaults.standard.bool(forKey: kUserTradeStyle)
    }
    
    func getCurrentTradeStyle() -> Bool {
        return UserDefaults.standard.bool(forKey: kUserTradeStyle)
    }
    
}
