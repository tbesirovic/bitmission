//
//  ApiService.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import SwiftWebSocket
import Reachability

final class ApiService {

    static let shared = ApiService()
    
    private init() {
        starInternetConnectionChangeNotifier()
    }
    
    //
    // MARK: Properties
    //
    
    let chars = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-=().,!"
    let url = "wss://api2.poloniex.com"
    let tickerChannel = "1002"
    let internetConnectionStatus = Reachability()!
    var webSocket: WebSocket?
    lazy var dataArray = [TickerModel]()
    
    //
    // MARK: Custom methods
    //
    
    private func starInternetConnectionChangeNotifier() {
        do {
            try internetConnectionStatus.startNotifier()
        } catch let error {
            #if DEBUG
                print("ApiService - starInternetConnectionChangeNotifier() => Start int. connection change notifier failed. = ", error.localizedDescription)
            #endif
        }
    }
    
    func subscribeWebSocket(fromView: UIView, completion: @escaping (TickerModel) -> ()) {
        webSocket?.open()
        webSocket = WebSocket(url)
        webSocket?.event.message = { message in
            if let text = message as? String {
                guard let index = (text.range(of: ",[")?.upperBound) else { return }
                let removeIdPart = String(text.suffix(from: index))
                let cuttedString = removeIdPart.filter { self.chars.contains($0) }
                let items = cuttedString.components(separatedBy: ",")
                let ticker = TickerModel(with: items)
                completion(ticker)
            }
        }
        webSocket?.send("{\"command\": \"subscribe\", \"channel\": \(tickerChannel)}")
    }
    
    func unsubscribeWebSocket() {
        webSocket?.send("{\"command\": \"unsubscribe\", \"channel\": \(tickerChannel)}")
        webSocket?.close()
    }
    
}
