//
//  FirebaseAuthService.swift
//  BitMission
//
//  Created by Timur Besirovic on 09/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import Foundation
import FirebaseAuth

final class FirebaseAuthService {

    /**
     Will attempt user registration with provided email and password. Registration results will be provided via completition closure.
     - parameters:
        - email: Email that will be used for registration
        - password: Password that will be used for registration
     */
    func signUpUser(withEmail email: String, andPassword password: String, completition: @escaping(_ isSuccess: Bool, _ errorMessage: String?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            let success = error == nil ? true : false
            let errorMessage = error == nil ? nil : error?.localizedDescription
            completition(success, errorMessage)
        }
    }
    
    /**
     Will attempt user login with provided email and password. Registration results will be provided via completition closure.
     - parameters:
        - email: Email that will be used for registration
        - password: Password that will be used for registration
     */
    func signInUser(withEmail email: String, andPassword password: String, completition: @escaping(_ isSuccess: Bool, _ errorMessage: String?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            let success = error == nil ? true : false
            let errorMessage = error == nil ? nil : error?.localizedDescription
            completition(success, errorMessage)
        }
    }
    
    /**
     Will attempt logout for current user with provided email and password. Registration results will be provided via completition closure.
     */
    func logoutUser(completition: (_ isSuccess: Bool, _ errorMessage: String?) -> Void) {
        do {
            try Auth.auth().signOut()
            completition(true, nil)
        } catch let error {
            completition(false, error.localizedDescription)
        }
    }
    
}
