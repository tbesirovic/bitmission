//
//  RegistrationViewModel.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import UIKit

final class RegistrationViewModel {
    
    //
    // MARK: - Properties
    //
    
    var fullName: String? { didSet { checkFormValidity() }}
    var email: String? { didSet { checkFormValidity() }}
    var password: String? { didSet { checkFormValidity() }}
    
    //
    // MARK: Completition handlers
    //
    
    var isFormValidObserver: ((Bool) -> ())?
    var isRegistrationObserver: ((Bool) -> ())?
    
    //
    // MARK: Custom methods
    //
    
    private func checkFormValidity() {
        guard let sFullName = fullName, let sEmail = email, let sPassword = password else {
            isFormValidObserver?(false)
            return
        }
        let isFormValid = !sFullName.isEmpty && !sEmail.isEmpty && !sPassword.isEmpty && sPassword.count >= 6 && sEmail.isValidAsEmail
        isFormValidObserver?(isFormValid)
    }
    
    func performRegistration(completion: @escaping (_ isSuccess: Bool, _ errorMessage: String?) -> ()) {
        guard let email = email, let password = password else { return }
        isRegistrationObserver?(true)
        FirebaseAuthService().signUpUser(withEmail: email, andPassword: password) { isSuccess, errorMessage in
            completion(isSuccess, errorMessage)
        }
    }
}


