//
//  LoginViewModel.swift
//  BitMission
//
//  Created by Timur Besirovic on 07/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import UIKit

final class LoginViewModel {
    
    //
    // MARK: - Properties
    //
    
    var email: String? { didSet { checkFormValidity() }}
    var password: String? { didSet { checkFormValidity() }}
    
    //
    // MARK: Completition handlers
    //
    
    var isFormValidObserver: ((Bool) -> ())?
    var isLoggingInObserver: ((Bool) -> ())?
    
    //
    // MARK: Custom methods
    //
    
    private func checkFormValidity() {
        guard let sEmail = email, let sPassword = password else {
            isFormValidObserver?(false)
            return
        }
        let isFormValid = !sEmail.isEmpty && !sPassword.isEmpty && sPassword.count >= 6 && sEmail.isValidAsEmail
        isFormValidObserver?(isFormValid)
    }
    
    func performLogin(completion: @escaping (_ isSuccess: Bool, _ errorMessage: String?) -> Void) {
        guard let email = email, let password = password else { return }
        isLoggingInObserver?(true)
        FirebaseAuthService().signInUser(withEmail: email, andPassword: password) { isSuccess, errorMessage in
            completion(isSuccess, errorMessage)
        }
    }
}
