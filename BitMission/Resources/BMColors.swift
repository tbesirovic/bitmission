//
//  BMColors.swift
//  BitMission
//
//  Created by Timur Besirovic on 06/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import UIKit

struct BMColors {
    static var brand: UIColor { return UIColor(red:59/255, green:89/255, blue:152/255, alpha:1.00) } // #1BB198
    static var brandLight: UIColor { return UIColor(red:139/255, green:157/255, blue:195/255, alpha:1.00) } // #ACD7D7
    static var brandSuperLight: UIColor { return UIColor(red:223/255, green:227/255, blue:238/255, alpha:1.00) } // #dfe3ee
}

