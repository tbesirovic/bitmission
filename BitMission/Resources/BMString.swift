//
//  BMString.swift
//  BitMission
//
//  Created by Timur Besirovic on 06/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import Foundation

struct BMString {
    
    // Button Strings
    
    static var login: String { return "Login" }
    static var register: String { return "Register" }
    static var enterLimitPlaceHolder: String { return "Enter limit value"}
    static var logOut: String { return "Logout"}
    static var switchView: String { return "Switch View"}
    static var goBack: String { return "Go back"}
    static var imANewMember: String { return "I'm a new member"}
    static var enterFullName: String { return "Enter full name"}
    static var enterEmail: String { return "Enter email"}
    static var enterPassword: String { return "Enter password"}
    static var defaultView: String {return "Default view"}
    static var customView: String {return "Custom View"}

    // Image names
    
    static var greenArrowUp: String { return "greenArrowUp"}
    static var RedArrowDown: String { return "RedArrowDown"}
    
    // Placeholders
    
    static var ask: String { return "ASK"}
    static var bid: String { return "BID"}
    static var tradeHighlight: String { return "Trade Highlight"}
    
    // Other
    
    static var alertDone: String { return "Done" }
    static var alertSettings: String { return "Settings" }
    static var errorNoInternetConnection: String { return "No internet connection" }
}
