//
//  Extensions.swift
//  BitMission
//
//  Created by Timur Besirovic on 09/02/2019.
//  Copyright © 2019 Timur Besirovic. All rights reserved.
//

import UIKit

//
// MARK: - String
//

extension String {
    var isValidAsEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluate(with: self)
    }
}

//
// MARK: - UIAlertController
//

extension UIAlertController {
    static func getSimpleAlert(withMessage message: String) -> UIAlertController {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let doneAction = UIAlertAction(title: BMString.alertDone, style: .default, handler: nil)
        alert.addAction(doneAction)
        return alert
    }
}
